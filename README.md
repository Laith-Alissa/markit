# Markit interview test solution

See ```doc/assignment.txt``` for details

Implement a basic feed reader/parser with some helper methods to calculate returns.

The Google API wasn't documented, so I wasn't able tom implement a particularly nice interface.

There's no active application you can run, since this is just a coding demo, though you can run
a couple of basic tests with ```mvn test```. The tests don't really test much since I couldn't
be bothered to write mocks, rather they're just there to prove the code executes without erroring.
