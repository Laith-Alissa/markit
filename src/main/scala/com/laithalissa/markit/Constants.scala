package com.laithalissa.markit

import java.time.format.DateTimeFormatter
import scala.concurrent.duration.DAYS
import scala.concurrent.duration.Duration

object Constants {
  final val API_URL = "https://www.google.com/finance/historical"

  private final val TICKER_DATE_FORMAT = "d-MMM-yy"
  final val dateFormatter = DateTimeFormatter.ofPattern(TICKER_DATE_FORMAT)

  final val DAY = Duration(1, DAYS)
  final val YEAR = Duration(366, DAYS)
}
