package com.laithalissa.markit

import java.time.LocalDate
import java.time.temporal.ChronoUnit.{DAYS, YEARS}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpResponse, HttpRequest, StatusCodes, Uri}
import akka.http.scaladsl.unmarshalling._
import akka.stream.ActorMaterializer


trait StockFetching {
  this: FeedParsing =>

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  def doRequest(uri: String): Future[HttpResponse] = {
    Http().singleRequest(HttpRequest(uri = uri))
  }

  /**
   * Get the feed from the api, and parse it into TickerRow items
   */
  private def getFeed(
    ticker: Seq[String],
    since: LocalDate
  ): Future[Seq[TickerRow]] = {
    val uri = StockFetching.buildUri(ticker = ticker, since = since)

    doRequest(uri.toString()) flatMap {
      response: HttpResponse => {
        // TODO: Handle redirects
        if (!response.status.isSuccess) {
          throw new RuntimeException(s"Server returned status code ${response.status.intValue()}")
        }

        val result = Unmarshal(response.entity).to[String]
        result map parseFeed
      }
    }
  }

  /**
   * Convenience method to get the last year's worth of feeds
   */
  private def getOneYearFeed(ticker: Seq[String]): Future[Seq[TickerRow]] = {
    val lastYear = LocalDate.now().minus(1, YEARS)
    getFeed(ticker, since = lastYear)
  }

  /**
   * 1) 1 year historic prices given a ticker
   */
  def dailyPrices(ticker: Seq[String]) : Future[Seq[Double]] = {
    getOneYearFeed(ticker) map { _.map { _.close } }
  }

  /**
   * 2) Daily returns, where return = (Price_Today – Price_Yesterday)/Price_Yesterday
   */
  def returns(ticker: Seq[String]): Future[Seq[Double]] = {
    getOneYearFeed(ticker) map {
      tickerRows: Seq[TickerRow] => {
        val prices = tickerRows map { _.close }
        val yesterdaysPrices = prices.drop(1) :+ 0.0

        (prices zip yesterdaysPrices) map {
          case (today: Double, yesterday: Double) => (today - yesterday) / yesterday
        } dropRight(1)
      }
    }
  }

  /**
   * 3) 1 year mean returns
   */
  def meanReturn(ticker: Seq[String]): Future[Double] = returns(ticker) map {
    returns: Seq[Double] => returns.sum / returns.length
  }
}

object StockFetching {

  /**
   * Build a uri for getting rates from the yahoo api
   *
   * @param ticker A sequence of company ticker symbols you wish to query for
   * @param since The time from which you want to get results
   */
  private def buildUri(
    ticker: Seq[String],
    since: LocalDate
  ): Uri = {
    val now = LocalDate.now()
    val daysAgo: Long = DAYS.between(since, now)

    Uri(Constants.API_URL).withQuery(
      Uri.Query(
        Map(
          "q"      -> s"NASDAQ:${ticker mkString " "}",
          "i"      -> s"${Constants.DAY.toSeconds}",
          "p"      -> s"${daysAgo}d",
          "output" -> "csv"
        )
      )
    )
  }
}

class StockFetcher extends StockFetching with FeedParsing
