package com.laithalissa.markit

import java.time.LocalDate

case class TickerRow(
  date: LocalDate,
  open: Double,
  high: Double,
  low: Double,
  close: Double,
  volume: Int
)

object TickerRow {
  final val DATE_FIELD = "Date"
  final val OPEN_FIELD = "Open"
  final val HIGH_FIELD = "High"
  final val LOW_FIELD = "Low"
  final val CLOSE_FIELD = "Close"
  final val VOLUME_FIELD = "Volume"

  def apply(row: Map[String, String]): TickerRow = {
    val d = LocalDate.parse(row(DATE_FIELD), Constants.dateFormatter)

    TickerRow(
      date   = d,
      open   = row(OPEN_FIELD  ).toDouble,
      high   = row(HIGH_FIELD  ).toDouble,
      low    = row(LOW_FIELD   ).toDouble,
      close  = row(CLOSE_FIELD ).toDouble,
      volume = row(VOLUME_FIELD).toInt
    )
  }
}
