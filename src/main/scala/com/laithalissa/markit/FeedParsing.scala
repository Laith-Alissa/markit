package com.laithalissa.markit

import java.io.StringReader

import com.github.tototoshi.csv.CSVReader

trait FeedParsing {
  /**
   * Parse the stock feed into TickerRows and sort by most recent
   */
  def parseFeed(feed: String): Seq[TickerRow] = {
    val reader = CSVReader.open(new StringReader(feed))
    reader.allWithHeaders() map {
      row: Map[String, String] => TickerRow(row)
    } sortBy { -_.date.toEpochDay }
  }
}
