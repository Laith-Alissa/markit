package com.laithalissa.markit

import scala.concurrent.ExecutionContext.Implicits.global

import org.scalatest.AsyncFlatSpec

class ApiSpec extends AsyncFlatSpec {
  behavior of "StockFetching"

  val stockFetcher = new StockFetcher()
  val ticker = Seq("GOOG")

  // Not a real test, but I couldn't be bothered to write mocks
  it should "fetch stocks for the last year" in {
    stockFetcher.dailyPrices(ticker = ticker) map {
      result: Seq[Double] => assert(result.nonEmpty)
    }
  }

  // this test is racey, but oh well.
  it should "calculate returns correctly" in {
    stockFetcher.returns(ticker) flatMap {
      returns: Seq[Double] => {
        stockFetcher.dailyPrices(ticker = ticker) map {
          prices: Seq[Double] => {
            val todaysPrice = prices(0)
            val yesterdaysPrice = prices(1)
            assert(returns(0) === (todaysPrice - yesterdaysPrice) / yesterdaysPrice)
          }
        }
      }
    }
  }

  // Not a real test, but I couldn't be bothered to write mocks
  it should "calculate mean return correctly" in {
    stockFetcher.meanReturn(ticker) map {
      returns: Double => assert(true)
    }
  }
}
